package tea.test

/**
 * 路由测试
*/

import std.unittest.testmacro.*

class RouteTestCase {
    RouteTestCase(
        public let url!: String = "",
        public let params!: ?ArrayList<String> = None,
        public var matched!: Bool = false,
        public var partialCheck!: Bool = false
    ) {}
}

class RouteCaseCollection {
    RouteCaseCollection (
        public let pattern: String,
        public let testCases: Array<RouteTestCase>
    ) {}
}

let benchmarkCases = ArrayList<RouteCaseCollection>()
let routeTestCases = ArrayList<RouteCaseCollection>()

func initTestCases() {
    benchmarkCases.add(all:
        [
            RouteCaseCollection(
                "/api/v1/const",
                [
                    RouteTestCase(url: "/api/v1/const", params: ArrayList<String>(), matched: true),
                    RouteTestCase(url: "/api/v1", matched: false),
                    RouteTestCase(url: "/api/v1", matched: false),
                    RouteTestCase(url: "/api/v1/something", matched: false)
                ]
            ),
            RouteCaseCollection(
                "/api/:param/fixedEnd",
                [
                    RouteTestCase(url: "/api/abc/fixedEnd", params: ArrayList<String>(["abc"]), matched: true),
                    RouteTestCase(url: "/api/abc/def/fixedEnd", matched: false)
                ]
            )
        ]
    )

    routeTestCases.add(all: benchmarkCases)
    routeTestCases.add(all: [
        RouteCaseCollection(
            "/api/v1/:param/+",
            [
                RouteTestCase(url: "/api/v1/entity", matched: false),
                RouteTestCase(url: "/api/v1/entity/", matched: false),
                RouteTestCase(url: "/api/v1/entity/1", params: ArrayList<String>(["entity", "1"]), matched: true),
                RouteTestCase(url: "/api/v", matched: false),
                RouteTestCase(url: "/api/v2", matched: false),
                RouteTestCase(url: "/api/v1/", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/api/v1/:param?",
            [
                RouteTestCase(url: "/api/v1", params: ArrayList<String>([""]), matched: true),
                RouteTestCase(url: "/api/v1/", params: ArrayList<String>([""]), matched: true),
                RouteTestCase(url: "/api/v1/optional", params: ArrayList<String>(["optional"]), matched: true),
                RouteTestCase(url: "/api/v", matched: false),
                RouteTestCase(url: "/api/v2", matched: false),
                RouteTestCase(url: "/api/xyz", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/v1/some/resource/name\\:customVerb",
            [
                RouteTestCase(url: "/v1/some/resource/name:customVerb", matched: true),
                RouteTestCase(url: "/v1/some/resource/name:test", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/v1/some/resource/:name\\:customVerb",
            [
                RouteTestCase(url: "/v1/some/resource/test:customVerb", params: ArrayList<String>(["test"]), matched: true),
                RouteTestCase(url: "/v1/some/resource/test:test", matched: false)
            ]
        ),
        RouteCaseCollection(
            #"/v1/some/resource/name\\:customVerb?\?/:param/*"#,
            [
                RouteTestCase(
                    url: "/v1/some/resource/name:customVerb??/test/optionalWildCard/character",
                    params: ArrayList<String>(["test", "optionalWildCard/character"]),
                    matched: true
                ),
                RouteTestCase(
                    url: "/v1/some/resource/name:customVerb??/test",
                    params: ArrayList<String>(["test", ""]),
                    matched: true
                )
            ]
        ),
        RouteCaseCollection(
            "/api/v1/*",
            [
                RouteTestCase(url: "/api/v1", params: ArrayList<String>([""]), matched: true),
                RouteTestCase(url: "/api/v1/", params: ArrayList<String>([""]), matched: true),
                RouteTestCase(url: "/api/v1/entity", params: ArrayList<String>(["entity"]), matched: true),
                RouteTestCase(url: "/api/v1/entity/1/2", params: ArrayList<String>(["entity/1/2"]), matched: true),
                RouteTestCase(url: "/api/v1/Entity/1/2", params: ArrayList<String>(["Entity/1/2"]), matched: true),
                RouteTestCase(url: "/api/v", matched: false),
                RouteTestCase(url: "/api/v2", matched: false),
                RouteTestCase(url: "/api/abc", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/api/v1/:param",
            [
                RouteTestCase(url: "/api/v1/entity", params: ArrayList<String>(["entity"]), matched: true),
                RouteTestCase(url: "/api/v1/entity/8728382", matched: false),
                RouteTestCase(url: "/api/v1", matched: false),
                RouteTestCase(url: "/api/v1/", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/api/v1/:param-:param2",
            [
                RouteTestCase(url: "/api/v1/entity-entity2", params: ArrayList<String>(["entity", "entity2"]), matched: true),
                RouteTestCase(url: "/api/v1/entity/8728382", matched: false),
                RouteTestCase(url: "/api/v1/entity-8728382", params: ArrayList<String>(["entity", "8728382"]), matched: true),
                RouteTestCase(url: "/api/v1", matched: false),
                RouteTestCase(url: "/api/v1/", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/api/v1/:filename.:extension",
            [
                RouteTestCase(url: "/api/v1/test.pdf", params: ArrayList<String>(["test", "pdf"]), matched: true),
                RouteTestCase(url: "/api/v1/test/pdf", matched: false),
                RouteTestCase(url: "/api/v1/test-pdf", matched: false),
                RouteTestCase(url: "/api/v1/test_pdf", matched: false),
                RouteTestCase(url: "/api/v1", matched: false),
                RouteTestCase(url: "/api/v1/", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/shop/product/::filter/color::color/size::size",
            [
                RouteTestCase(
                    url: "/shop/product/:test/color:blue/size:xs",
                    params: ArrayList<String>(["test", "blue", "xs"]),
                    matched: true
                ),
                RouteTestCase(url: "/shop/product/test/color:blue/size:xs", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/::param?",
            [
                RouteTestCase(url: "/:hello", params: ArrayList<String>(["hello"]), matched: true),
                RouteTestCase(url: "/:", params: ArrayList<String>([""]), matched: true),
                RouteTestCase(url: "/", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/test:sign:param",
            [
                RouteTestCase(url: "/test-abc", params: ArrayList<String>(["-", "abc"]), matched: true),
                RouteTestCase(url: "/test", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/:param1:param2?:param3",
            [
                RouteTestCase(url: "/abbbc", params: ArrayList<String>(["a", "b", "bbc"]), matched: true),
                // {url: "/ac", params: ArrayList<String>(["a", "", "c"]), matched: true}, // TODO: fix it
                RouteTestCase(url: "/test", params: ArrayList<String>(["t", "e", "st"]), matched: true)
            ]
        ),
        RouteCaseCollection(
            "/test:optional?:mandatory",
            [
                // {url: "/testo", params: ArrayList<String>(["", "o"]), matched: true}, // TODO: fix it
                RouteTestCase(url: "/testoaaa", params: ArrayList<String>(["o", "aaa"]), matched: true),
                RouteTestCase(url: "/test", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/test:optional?:optional2?",
            [
                RouteTestCase(url: "/testo", params: ArrayList<String>(["o", ""]), matched: true),
                RouteTestCase(url: "/testoaaa", params: ArrayList<String>(["o", "aaa"]), matched: true),
                RouteTestCase(url: "/test", params: ArrayList<String>(["", ""]), matched: true),
                RouteTestCase(url: "/tes", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/foo:param?bar",
            [
                RouteTestCase(url: "/foofaselbar", params: ArrayList<String>(["fasel"]), matched: true),
                RouteTestCase(url: "/foobar", params: ArrayList<String>([""]), matched: true),
                RouteTestCase(url: "/fooba", matched: false),
                RouteTestCase(url: "/fobar", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/foo*bar",
            [
                RouteTestCase(url: "/foofaselbar", params: ArrayList<String>(["fasel"]), matched: true),
                RouteTestCase(url: "/foobar", params: ArrayList<String>([""]), matched: true),
                RouteTestCase(url: "/", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/foo+bar",
            [
                RouteTestCase(url: "/foofaselbar", params: ArrayList<String>(["fasel"]), matched: true),
                RouteTestCase(url: "/foobar", matched: false),
                RouteTestCase(url: "/", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/a*cde*g/",
            [
                RouteTestCase(url: "/abbbcdefffg", params: ArrayList<String>(["bbb", "fff"]), matched: true),
                RouteTestCase(url: "/acdeg", params: ArrayList<String>(["", ""]), matched: true),
                RouteTestCase(url: "/", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/*v1*/proxy",
            [
                RouteTestCase(url: "/customer/v1/cart/proxy", params: ArrayList<String>(["customer/", "/cart"]), matched: true),
                RouteTestCase(url: "/v1/proxy", params: ArrayList<String>(["", ""]), matched: true),
                RouteTestCase(url: "/v1/", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/foo***bar",
            [
                RouteTestCase(url: "/foo*abar", params: ArrayList<String>(["*a", "", ""]), matched: true),
                RouteTestCase(url: "/foo*bar", params: ArrayList<String>(["*", "", ""]), matched: true),
                RouteTestCase(url: "/foobar", params: ArrayList<String>(["", "", ""]), matched: true),
                RouteTestCase(url: "/fooba", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/name::name",
            [
                RouteTestCase(url: "/name:john", params: ArrayList<String>(["john"]), matched: true)
            ]
        ),
        RouteCaseCollection(
            "/@:name",
            [
                RouteTestCase(url: "/@john", params: ArrayList<String>(["john"]), matched: true)
            ]
        ),
        RouteCaseCollection(
            "/-:name",
            [
                RouteTestCase(url: "/-john", params: ArrayList<String>(["john"]), matched: true)
            ]
        ),
        RouteCaseCollection(
            "/.:name",
            [
                RouteTestCase(url: "/.john", params: ArrayList<String>(["john"]), matched: true)
            ]
        ),
        RouteCaseCollection(
            "/api/v1/:param/abc/*",
            [
                RouteTestCase(url: "/api/v1/well/abc/wildcard", params: ArrayList<String>(["well", "wildcard"]), matched: true),
                RouteTestCase(url: "/api/v1/well/abc/", params: ArrayList<String>(["well", ""]), matched: true),
                RouteTestCase(url: "/api/v1/well/abc", params: ArrayList<String>(["well", ""]), matched: true),
                RouteTestCase(url: "/api/v1/well/ttt", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/api/:day/:month?/:year?",
            [
                RouteTestCase(url: "/api/1",   params: ArrayList<String>(["1", "", ""]), matched: true),
                RouteTestCase(url: "/api/1/",  params: ArrayList<String>(["1", "", ""]), matched: true),
                RouteTestCase(url: "/api/1//", params: ArrayList<String>(["1", "", ""]), matched: true),
                RouteTestCase(url: "/api/1/-/", params: ArrayList<String>(["1", "-", ""]), matched: true),
                RouteTestCase(url: "/api/1-",  params: ArrayList<String>(["1-", "", ""]), matched: true),
                RouteTestCase(url: "/api/1.",  params: ArrayList<String>(["1.", "", ""]), matched: true),
                RouteTestCase(url: "/api/1/2", params: ArrayList<String>(["1", "2", ""]), matched: true),
                RouteTestCase(url: "/api/1/2/3", params: ArrayList<String>(["1", "2", "3"]), matched: true),
                RouteTestCase(url: "/api/", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/api/:day.:month?.:year?",
            [
                RouteTestCase(url: "/api/1",    matched: false),
                RouteTestCase(url: "/api/1/",   matched: false),
                RouteTestCase(url: "/api/1.",   matched: false),
                RouteTestCase(url: "/api/1..",  params: ArrayList<String>(["1", "", ""]), matched: true),
                RouteTestCase(url: "/api/1.2",  matched: false),
                RouteTestCase(url: "/api/1.2.", params: ArrayList<String>(["1", "2", ""]), matched: true),
                RouteTestCase(url: "/api/1.2.3", params: ArrayList<String>(["1", "2", "3"]), matched: true),
                RouteTestCase(url: "/api/",     matched: false)
            ]
        ),
        RouteCaseCollection(
            "/api/:day-:month?-:year?",
            [
                RouteTestCase(url: "/api/1",    matched: false),
                RouteTestCase(url: "/api/1/",   matched: false),
                RouteTestCase(url: "/api/1-",   matched: false),
                RouteTestCase(url: "/api/1--",  params: ArrayList<String>(["1", "", ""]), matched: true),
                RouteTestCase(url: "/api/1-/",  matched: false),
                // {url: "/api/1-/-", matched: false}, // TODO: fix this part
                RouteTestCase(url: "/api/1-2",  matched: false),
                RouteTestCase(url: "/api/1-2-", params: ArrayList<String>(["1", "2", ""]), matched: true),
                RouteTestCase(url: "/api/1-2-3", params: ArrayList<String>(["1", "2", "3"]), matched: true),
                RouteTestCase(url: "/api/",     matched: false)
            ]
        ),
        RouteCaseCollection(
            "/api/*",
            [
                RouteTestCase(url: "/api/",          params: ArrayList<String>([""]), matched: true),
                RouteTestCase(url: "/api/joker",     params: ArrayList<String>(["joker"]), matched: true),
                RouteTestCase(url: "/api",           params: ArrayList<String>([""]), matched: true),
                RouteTestCase(url: "/api/v1/entity", params: ArrayList<String>(["v1/entity"]), matched: true),
                RouteTestCase(url: "/api2/v1/entity", matched: false),
                RouteTestCase(url: "/api_ignore/v1/entity", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/partialCheck/foo/bar/:param",
            [
                RouteTestCase(url: "/partialCheck/foo/bar/test",       params: ArrayList<String>(["test"]), matched: true, partialCheck: true),
                RouteTestCase(url: "/partialCheck/foo/bar/test/test2", params: ArrayList<String>(["test"]), matched: true, partialCheck: true),
                RouteTestCase(url: "/partialCheck/foo/bar",            matched: false, partialCheck: true),
                RouteTestCase(url: "/partiaFoo",                       matched: false, partialCheck: true)
            ]
        ),
        RouteCaseCollection(
            "/",
            [
                RouteTestCase(url: "/api", matched: false),
                RouteTestCase(url: "", params: ArrayList<String>(), matched: true),
                RouteTestCase(url: "/", params: ArrayList<String>(), matched: true)
            ]
        ),
        RouteCaseCollection(
            "/config/abc.json",
            [
                RouteTestCase(url: "/config/abc.json", params: ArrayList<String>(), matched: true),
                RouteTestCase(url: "config/abc.json",  matched: false),
                RouteTestCase(url: "/config/efg.json", matched: false),
                RouteTestCase(url: "/config",          matched: false)
            ]
        ),
        RouteCaseCollection(
            "/config/*.json",
            [
                RouteTestCase(url: "/config/abc.json", params: ArrayList<String>(["abc"]), matched: true),
                RouteTestCase(url: "/config/efg.json", params: ArrayList<String>(["efg"]), matched: true),
                RouteTestCase(url: "/config/.json",    params: ArrayList<String>([""]), matched: true),
                RouteTestCase(url: "/config/efg.csv",  matched: false),
                RouteTestCase(url: "config/abc.json",  matched: false),
                RouteTestCase(url: "/config",          matched: false)
            ]
        ),
        RouteCaseCollection(
            "/config/+.json",
            [
                RouteTestCase(url: "/config/abc.json", params: ArrayList<String>(["abc"]), matched: true),
                RouteTestCase(url: "/config/.json",    matched: false),
                RouteTestCase(url: "/config/efg.json", params: ArrayList<String>(["efg"]), matched: true),
                RouteTestCase(url: "/config/efg.csv",  matched: false),
                RouteTestCase(url: "config/abc.json",  matched: false),
                RouteTestCase(url: "/config",          matched: false)
            ]
        ),
        RouteCaseCollection(
            "/xyz",
            [
                RouteTestCase(url: "xyz",  matched: false),
                RouteTestCase(url: "xyz/", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/api/*/:param?",
            [
                RouteTestCase(url: "/api/",                    params: ArrayList<String>(["", ""]), matched: true),
                RouteTestCase(url: "/api/joker",               params: ArrayList<String>(["joker", ""]), matched: true),
                RouteTestCase(url: "/api/joker/batman",        params: ArrayList<String>(["joker", "batman"]), matched: true),
                RouteTestCase(url: "/api/joker//batman",       params: ArrayList<String>(["joker/", "batman"]), matched: true),
                RouteTestCase(url: "/api/joker/batman/robin",  params: ArrayList<String>(["joker/batman", "robin"]), matched: true),
                RouteTestCase(url: "/api/joker/batman/robin/1", params: ArrayList<String>(["joker/batman/robin", "1"]), matched: true),
                RouteTestCase(url: "/api/joker/batman/robin/1/", params: ArrayList<String>(["joker/batman/robin/1", ""]), matched: true),
                RouteTestCase(url: "/api/joker-batman/robin/1",   params: ArrayList<String>(["joker-batman/robin", "1"]), matched: true),
                RouteTestCase(url: "/api/joker-batman-robin/1",   params: ArrayList<String>(["joker-batman-robin", "1"]), matched: true),
                RouteTestCase(url: "/api/joker-batman-robin-1",   params: ArrayList<String>(["joker-batman-robin-1", ""]), matched: true),
                RouteTestCase(url: "/api", params: ArrayList<String>(["", ""]), matched: true)
            ]
        ),
        RouteCaseCollection(
            "/api/*/:param",
            [
                RouteTestCase(url: "/api/test/abc",             params: ArrayList<String>(["test", "abc"]), matched: true),
                RouteTestCase(url: "/api/joker/batman",         params: ArrayList<String>(["joker", "batman"]), matched: true),
                RouteTestCase(url: "/api/joker/batman/robin",   params: ArrayList<String>(["joker/batman", "robin"]), matched: true),
                RouteTestCase(url: "/api/joker/batman/robin/1", params: ArrayList<String>(["joker/batman/robin", "1"]), matched: true),
                RouteTestCase(url: "/api/joker/batman-robin/1", params: ArrayList<String>(["joker/batman-robin", "1"]), matched: true),
                RouteTestCase(url: "/api/joker-batman-robin-1", matched: false),
                RouteTestCase(url: "/api", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/api/+/:param",
            [
                RouteTestCase(url: "/api/test/abc", params: ArrayList<String>(["test", "abc"]), matched: true),
                RouteTestCase(url: "/api/joker/batman/robin/1", params: ArrayList<String>(["joker/batman/robin", "1"]), matched: true),
                RouteTestCase(url: "/api/joker", matched: false),
                RouteTestCase(url: "/api", matched: false)
            ]
        ),
        RouteCaseCollection(
            "/api/*/:param/:param2",
            [
                RouteTestCase(url: "/api/test/abc/1", params: ArrayList<String>(["test", "abc", "1"]), matched: true),
                RouteTestCase(url: "/api/joker/batman", matched: false),
                RouteTestCase(url: "/api/joker/batman-robin/1", params: ArrayList<String>(["joker", "batman-robin", "1"]), matched: true),
                RouteTestCase(url: "/api/joker-batman-robin-1", matched: false),
                RouteTestCase(url: "/api/test/abc", matched: false),
                RouteTestCase(url: "/api/joker/batman/robin", params: ArrayList<String>(["joker", "batman", "robin"]), matched: true),
                RouteTestCase(url: "/api/joker/batman/robin/1", params: ArrayList<String>(["joker/batman", "robin", "1"]), matched: true),
                RouteTestCase(url: "/api/joker/batman/robin/1/2", params: ArrayList<String>(["joker/batman/robin", "1", "2"]), matched: true),
                RouteTestCase(url: "/api", matched: false),
                RouteTestCase(url: "/api/:test", matched: false)
            ]
        )
    ])
}

func isSameRouteSegment(rs1: RouteSegment, rs2: RouteSegment): Bool {
    // TODO 检查 Constraint
    return rs1.constant == rs2.constant &&
        rs1.paramName == rs2.paramName &&
        rs1.comparePart == rs2.comparePart &&
        rs1.partCount == rs2.partCount &&
        rs1.length == rs2.length &&
        rs1.isParam == rs2.isParam &&
        rs1.isGreedy == rs2.isGreedy &&
        rs1.isOptional == rs2.isOptional &&
        rs1.isLast == rs2.isLast &&
        rs1.hasOptionalSlash == rs2.hasOptionalSlash
}

func isSameRouteParser(rp1: RouteParser, rp2: RouteParser): Bool {
    if (rp1.segments.size != rp2.segments.size) {
        return false
    }

    for (i in 0..rp1.segments.size) {
        if (!isSameRouteSegment(rp1.segments[i], rp2.segments[i])) {
            return false
        }
    }

    if (rp1.params != rp2.params) {
        return false
    }

    if (rp1.wildCardCount != rp2.wildCardCount) {
        return false
    }

    if (rp1.plusCount != rp2.plusCount) {
        return false
    }

    return true
}

@Test
class PathTester {
    static init() {
        initTestCases()
    }
    
    @TestCase
    func testParseRoute(): Unit {
        var rp = RouteParser()

        rp = parseRoute("/shop/product/::filter/color::color/size::size")
        @Assert(isSameRouteParser(
            rp,
            RouteParser(
                segments: ArrayList<RouteSegment>([
                    RouteSegment(constant: "/shop/product/:", length: 15),
                    RouteSegment(isParam: true, paramName: "filter", comparePart: "/color:", partCount: 1),
                    RouteSegment(constant: "/color:", length: 7),
                    RouteSegment(isParam: true, paramName: "color", comparePart: "/size:", partCount: 1),
                    RouteSegment(constant: "/size:", length: 6),
                    RouteSegment(isParam: true, paramName: "size", isLast: true)
                ]),
                params: ArrayList<String>(["filter", "color", "size"])
            )
        ), true)

        rp = parseRoute("/api/v1/:param/abc/*")
        @Assert(isSameRouteParser(
            rp,
            RouteParser(
                segments: ArrayList<RouteSegment>([
                    RouteSegment(constant: "/api/v1/", length: 8),
                    RouteSegment(isParam: true, paramName: "param", comparePart: "/abc", partCount: 1),
                    RouteSegment(constant: "/abc/", length: 5, hasOptionalSlash: true),
                    RouteSegment(isParam: true, paramName: "*1", isGreedy: true, isOptional: true, isLast: true)
                ]),
                params: ArrayList<String>(["param", "*1"]),
                wildCardCount: 1
            )
        ), true)

        rp = parseRoute("/v1/some/resource/name\\:customVerb")
        @Assert(isSameRouteParser(
            rp,
            RouteParser(
                segments: ArrayList<RouteSegment>([
                    RouteSegment(constant: "/v1/some/resource/name:customVerb", length: 33, isLast: true)
                ])
            )
        ), true)

        rp = parseRoute("/v1/some/resource/:name\\:customVerb")
        @Assert(isSameRouteParser(
            rp,
            RouteParser(
                segments: ArrayList<RouteSegment>([
                    RouteSegment(constant: "/v1/some/resource/", length: 18),
                    RouteSegment(isParam: true, paramName: "name", comparePart: ":customVerb", partCount: 1),
                    RouteSegment(constant: ":customVerb", length: 11, isLast: true)
                ]),
                params: ArrayList<String>(["name"])
            )
        ), true)

        rp = parseRoute("/v1/some/resource/name\\\\:customVerb?\\?/:param/*")
        @Assert(isSameRouteParser(
            rp,
            RouteParser(
                segments: ArrayList<RouteSegment>([
                    RouteSegment(constant: "/v1/some/resource/name:customVerb??/", length: 36),
                    RouteSegment(isParam: true, paramName: "param", comparePart: "/", partCount: 1),
                    RouteSegment(constant: "/", length: 1, hasOptionalSlash: true),
                    RouteSegment(isParam: true, paramName: "*1", isGreedy: true, isOptional: true, isLast: true)
                ]),
                params: ArrayList<String>(["param", "*1"]),
                wildCardCount: 1
            )
        ), true)

        rp = parseRoute("/api/*/:param/:param2")
        @Assert(isSameRouteParser(
            rp,
            RouteParser(
                segments: ArrayList<RouteSegment>([
                    RouteSegment(constant: "/api/", length: 5, hasOptionalSlash: true),
                    RouteSegment(isParam: true, paramName: "*1", isGreedy: true, isOptional: true, comparePart: "/", partCount: 2),
                    RouteSegment(constant: "/", length: 1),
                    RouteSegment(isParam: true, paramName: "param", comparePart: "/", partCount: 1),
                    RouteSegment(constant: "/", length: 1),
                    RouteSegment(isParam: true, paramName: "param2", isLast: true)
                ]),
                params: ArrayList<String>(["*1", "param", "param2"]),
                wildCardCount: 1
            )
        ), true)

        rp = parseRoute("/test:optional?:optional2?")
        @Assert(isSameRouteParser(
            rp,
            RouteParser(
                segments: ArrayList<RouteSegment>([
                    RouteSegment(constant: "/test", length: 5),
                    RouteSegment(isParam: true, paramName: "optional", isOptional: true, length: 1),
                    RouteSegment(isParam: true, paramName: "optional2", isOptional: true, isLast: true)
                ]),
                params: ArrayList<String>(["optional", "optional2"])
            )
        ), true)

        rp = parseRoute("/config/+.json")
        @Assert(isSameRouteParser(
            rp,
            RouteParser(
                segments: ArrayList<RouteSegment>([
                    RouteSegment(constant: "/config/", length: 8),
                    RouteSegment(isParam: true, paramName: "+1", isGreedy: true, isOptional: false, comparePart: ".json", partCount: 1),
                    RouteSegment(constant: ".json", length: 5, isLast: true)
                ]),
                params: ArrayList<String>(["+1"]),
                plusCount: 1
            )
        ), true)

        rp = parseRoute("/api/:day.:month?.:year?")
        @Assert(isSameRouteParser(
            rp,
            RouteParser(
                segments: ArrayList<RouteSegment>([
                    RouteSegment(constant: "/api/", length: 5),
                    RouteSegment(isParam: true, paramName: "day", isOptional: false, comparePart: ".", partCount: 2),
                    RouteSegment(constant: ".", length: 1),
                    RouteSegment(isParam: true, paramName: "month", isOptional: true, comparePart: ".", partCount: 1),
                    RouteSegment(constant: ".", length: 1),
                    RouteSegment(isParam: true, paramName: "year", isOptional: true, isLast: true)
                ]),
                params: ArrayList<String>(["day", "month", "year"])
            )
        ), true)

        rp = parseRoute("/*v1*/proxy")
        @Assert(isSameRouteParser(
            rp,
            RouteParser(
                segments: ArrayList<RouteSegment>([
                    RouteSegment(constant: "/", length: 1, hasOptionalSlash: true),
                    RouteSegment(isParam: true, paramName: "*1", isGreedy: true, isOptional: true, comparePart: "v1", partCount: 1),
                    RouteSegment(constant: "v1", length: 2),
                    RouteSegment(isParam: true, paramName: "*2", isGreedy: true, isOptional: true, comparePart: "/proxy", partCount: 1),
                    RouteSegment(constant: "/proxy", length: 6, isLast: true)
                ]),
                params: ArrayList<String>(["*1", "*2"]),
                wildCardCount: 2
            )
        ), true)
    }

    @TestCase
    func testMatchParams(): Unit {
        let ctxParams = ArrayList<String>(30, {_ => ""})
        for (testCollection in routeTestCases) {
            let parser = parseRoute(testCollection.pattern)
            for (c in testCollection.testCases) {
                println("${c.url}")
                let matched = parser.getMatch(c.url, c.url, ctxParams, c.partialCheck)
                @Assert(c.matched, matched)
                if (matched) {
                    if (let Some(params) <- c.params) {
                        if (params.size > 0) {
                            @Assert(params, ctxParams[..params.size])
                        }
                    }
                }
            }
        }
    }
}

